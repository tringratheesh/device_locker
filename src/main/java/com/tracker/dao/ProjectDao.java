package com.tracker.dao;

import java.util.List;

import com.tracker.datastore.Project;
import com.tracker.dto.ProjectDeleteDto;
import com.tracker.dto.ProjectDto;
import com.tracker.dto.ProjectUpdateDto;
import com.tracker.dto.response.ProjectDetail;

public interface ProjectDao {

	void createProject(ProjectDto dto, String email);

	List<ProjectDetail> getProjects();

	Boolean updateProject(ProjectUpdateDto dto, String email);

	Boolean deleteProject(ProjectDeleteDto dto, String email);
	
	Project getProject(long projectId);
	
	Project findProject(String key, String value);

}
