package com.tracker.dao;

import java.util.List;

import com.tracker.datastore.Device;
import com.tracker.dto.DeviceDto;
import com.tracker.dto.DeviceReturnDto;
import com.tracker.dto.response.DeviceResponseDto;

public interface DeviceDao {

	void createDevice(DeviceDto dto, String email);

	List<Object> getDevices();

	Boolean updateDevice(DeviceResponseDto dto, String email);

	Boolean deleteDevice(DeviceReturnDto dto, String email);
	
	Device getDeviceEntity(long deviceId);
	
	DeviceResponseDto getDeviceDetail(long deviceId);
	
	List<Object> getAvailableDevices();
	
	List<DeviceResponseDto> prepareDeviceList(List<Object> list);
	
	Device findDevice(String key, String value);
}
