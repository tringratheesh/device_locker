package com.tracker.dao;

import java.util.List;

import com.googlecode.objectify.Objectify;
import com.tracker.datastore.Locker;
import com.tracker.dto.LockerDto;

public interface LockerDao {

	void createLocker(LockerDto dto, Objectify ofy, long userId);
		
	Locker findLocker(String key, long value);
	
	List<Object> occupiedDevices();
}
