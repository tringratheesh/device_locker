package com.tracker.dao;

import java.util.List;

import com.tracker.datastore.Platform;
import com.tracker.dto.PlatformDeleteDto;
import com.tracker.dto.PlatformDto;
import com.tracker.dto.PlatformUpdateDto;

public interface PlatformDao {

	void createPlatform(PlatformDto dto, String email);

	List<PlatformUpdateDto> getPlatforms();

	Boolean updatePlatform(PlatformUpdateDto dto, String email);

	Boolean deletePlatform(PlatformDeleteDto dto, String email);
	
	Platform getPlatform(long platformId);
	
	Platform findPlatform(String key, String value);
}
