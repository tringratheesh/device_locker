package com.tracker.dao.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.dao.DeviceDao;
import com.tracker.datastore.Device;
import com.tracker.dto.DeviceDto;
import com.tracker.dto.DeviceReturnDto;
import com.tracker.dto.response.DeviceResponseDto;
import com.tracker.util.ObjectifyUtil;

@Component
public class DeviceDaImpl implements DeviceDao {

	@Autowired
	private ObjectifyUtil<Device> objUtil;

	@Override
	public void createDevice(DeviceDto dto, String email) {

		Device device = new Device();
		device.setName(dto.getName());
		device.setNickName(dto.getNickName());
		device.setColor(dto.getColor());
		device.setIpAddress(dto.getIpAddress());
		device.setUdid(dto.getUdid());
		device.setMacAddress(dto.getMacAddress());
		device.setCapacity(dto.getCapacity());
		device.setOsVersion(dto.getOsVersion());
		device.setComments(dto.getComments());
		device.setPlatformId(dto.getPlatformId());
		device.setEnabled(true);
		device.setImei(dto.getImei());
		device.setDensity(dto.getDensity());
		device.setResolution(dto.getResolution());
		device.setSize(dto.getSize());
		device.setRam(dto.getRam());

		// Audit fields
		device.setCreatedBy(email);
		device.setCreatedTime(System.currentTimeMillis());
		device.setUpdatedBy(email);
		device.setUpdatedTime(System.currentTimeMillis());
		device.setDeleted(false);

		objUtil.save(device);
	}

	@Override
	public List<Object> getDevices() {

		List<Object> list = objUtil.retriveList(Device.class, "isDeleted", false, "- createdTime");

		return list;
	}

	@Override
	public Boolean updateDevice(DeviceResponseDto dto, String email) {

		Device device = (Device) objUtil.retrive(Device.class, dto.getDeviceId());

		if (device != null) {
			device.setName(dto.getName());
			device.setNickName(dto.getNickName());
			device.setColor(dto.getColor());
			device.setIpAddress(dto.getIpAddress());
			device.setUdid(dto.getUdid());
			device.setMacAddress(dto.getMacAddress());
			device.setCapacity(dto.getCapacity());
			device.setOsVersion(dto.getOsVersion());
			device.setComments(dto.getComments());
			device.setPlatformId(dto.getPlatformId());
			device.setEnabled(dto.getIsAvailable());
			device.setImei(dto.getImei());
			device.setDensity(dto.getDensity());
			device.setResolution(dto.getResolution());
			device.setSize(dto.getSize());
			device.setRam(dto.getRam());

			// Audit fields
			device.setUpdatedBy(email);
			device.setUpdatedTime(System.currentTimeMillis());
			objUtil.save(device);
			
			return true;
		}
		return false;
	}

	@Override
	public Boolean deleteDevice(DeviceReturnDto dto, String email) {

		Device device = (Device) objUtil.retrive(Device.class, dto.getDeviceId());

		if (device != null) {
			device.setDeleted(true);

			// Audit fields
			device.setUpdatedBy(email);
			device.setUpdatedTime(System.currentTimeMillis());
			objUtil.save(device);
			
			return true;
		}
		return false;
	}

	@Override
	public Device getDeviceEntity(long deviceId) {

		return (Device) objUtil.retrive(Device.class, deviceId);
	}

	@Override
	public DeviceResponseDto getDeviceDetail(long deviceId) {
		
		Device device = getDeviceEntity(deviceId);
		DeviceResponseDto response = prepareDeviceResponse(device);

		return response;
	}
	
	@Override
	public List<Object> getAvailableDevices() {
		
		LinkedHashMap<String, Object> constraints = new LinkedHashMap<>();
		constraints.put("isDeleted", false);
		constraints.put("isEnabled", true);
		
		List<Object> list = objUtil.retriveList(Device.class, constraints, "-createdTime");
		return list;
	}
	
	@Override
	public List<DeviceResponseDto> prepareDeviceList(List<Object> list) {
		
		List<DeviceResponseDto> devices = new ArrayList<DeviceResponseDto>();

		for (Object obj : list) {
			Device device = (Device) obj;
			DeviceResponseDto dto = prepareDeviceResponse(device);
			devices.add(dto);
		}

		return devices;
	}
	
	private DeviceResponseDto prepareDeviceResponse(Device device) {
		
		DeviceResponseDto dto = new DeviceResponseDto();
		
		dto.setName(device.getName());
		dto.setDeviceId(device.getDeviceId());
		dto.setNickName(device.getNickName());
		dto.setColor(device.getColor());
		dto.setIpAddress(device.getIpAddress());
		dto.setUdid(device.getUdid());
		dto.setMacAddress(device.getMacAddress());
		dto.setCapacity(device.getCapacity());
		dto.setOsVersion(device.getOsVersion());
		dto.setComments(device.getComments());
		dto.setPlatformId(device.getPlatformId());
		dto.setIsAvailable(device.isEnabled());
		dto.setImei(device.getImei());
		dto.setDensity(device.getDensity());
		dto.setResolution(device.getResolution());
		dto.setSize(device.getSize());
		dto.setRam(device.getRam());

		return dto;
	}

	@Override
	public Device findDevice(String key, String value) {

		return (Device) objUtil.retrive(Device.class, key, value);
	}
}
