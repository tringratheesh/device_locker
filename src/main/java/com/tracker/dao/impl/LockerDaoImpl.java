package com.tracker.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.googlecode.objectify.Objectify;
import com.tracker.dao.LockerDao;
import com.tracker.datastore.Locker;
import com.tracker.dto.LockerDto;
import com.tracker.util.ObjectifyUtil;

@Component
public class LockerDaoImpl implements LockerDao {

	@Autowired
	private ObjectifyUtil<Locker> objUtil;

	@Override
	public void createLocker(LockerDto dto, Objectify ofy, long userId) {

		Locker locker = new Locker();
		
		locker.setDeviceId(dto.getDeviceId());
		locker.setProjectId(dto.getProjectId());
		locker.setUserId(userId);
		locker.setUpdatedTime(System.currentTimeMillis());

		ofy.save().entity(locker).now();
	}

	/*
	@Override
	public Boolean deleteLocker(Objectify ofy, long deviceId, User user) {

		LinkedHashMap<String, Object> constraints = new LinkedHashMap<>();

		constraints.put("deviceId", deviceId);
		if (user.getRole().contains(Constants.USER_ROLE)) {
			constraints.put("userId", user.getUserId());
		}
		
		Locker locker = (Locker) objUtil.retriveSingle(Locker.class, constraints);
		if (locker != null) {
			
			ofy.delete().entity(locker).now();
			return true;
		}
		return false;
	}
*/
	@Override
	public Locker findLocker(String key, long value) {

		return (Locker) objUtil.retrive(Locker.class, key, value);
	}

	@Override
	public List<Object> occupiedDevices() {

		List<Object> list = objUtil.retriveList(Locker.class, "- updatedTime");
		
		return list;
	}

}
