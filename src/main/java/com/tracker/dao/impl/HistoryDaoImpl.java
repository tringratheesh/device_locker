package com.tracker.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.tracker.constants.ServerConstants;
import com.tracker.dao.HistoryDao;
import com.tracker.datastore.History;

@Component
public class HistoryDaoImpl implements HistoryDao {

	@Override
	public void createHistory(Objectify ofy, long userId, long deviceId, long projectId, long bookedTime) {

		History history = new History();

		history.setDeviceId(deviceId);
		history.setUserId(userId);
		history.setProjectId(projectId);
		history.setCreatedTime(System.currentTimeMillis());
		
		history.setReturnedTime(System.currentTimeMillis());
		history.setBookedTime(bookedTime);
		
		/*
		 * if(!isBooked) { history.setReturnedTime(System.currentTimeMillis());
		 * history.setBookedTime(bookedTime); } else {
		 * history.setBookedTime(System.currentTimeMillis()); }
		 */
		ofy.save().entity(history).now();
	}

	/*
	@Override
	public List<History> getHistory(String pageNum1) {
		
		int pageNum = 1;
		int offSet = (pageNum == 1) ? (pageNum - 1) : (pageNum * ServerConstants.PAGE_LIMIT);
		
		com.googlecode.objectify.cmd.Query<History> query = ObjectifyService.ofy().load().type(History.class).filter("deviceId", "121312434535").order("- createdTime").limit(ServerConstants.PAGE_LIMIT)
				.offset(offSet);
		QueryResultIterator<History> iterator = query.iterator();
		List<History> list = new ArrayList<History>();
		while (iterator.hasNext()) {
			History history = iterator.next();
			list.add(history);
		}
		Logger.getLogger("info").warning("getHistory list = " + list);

		return list;
	}
	
	public void getPageHistory(String cursor) {
		
		com.googlecode.objectify.cmd.Query<History> query = ObjectifyService.ofy().load().type(History.class).limit(ServerConstants.PAGE_LIMIT);
				
		if (cursor != null && cursor.length() > 0 && !cursor.equals("=")) {
			Cursor cur = Cursor.fromWebSafeString(cursor);
			query.startAt(cur);
		}
		query.limit(ServerConstants.PAGE_LIMIT);
		boolean isContinue = false;
		
		QueryResultIterator<History> iterator = query.iterator();
		while (iterator.hasNext()) {
			History history = iterator.next();
			System.out.println("history === "+history.getDeviceId()+ "Booked time ="+history.getBookedTime());
			isContinue = true;
		}
		
		if (isContinue) {
			Cursor curs = iterator.getCursor();
			System.out.println("Cursor === "+curs.toWebSafeString());
		}
	}
*/

	@Override
	public int getAllHistoriesCount(long deviceId) {
		
		return ObjectifyService.ofy().load().type(History.class).filter("deviceId", deviceId).count();
	}

	@Override
	public List<History> getPageHistory(int pageNum, long deviceId) {

		int offSet = (pageNum - 1) * ServerConstants.PAGE_LIMIT;
		
		com.googlecode.objectify.cmd.Query<History> query = ObjectifyService.ofy().load().type(History.class).filter("deviceId", deviceId).limit(ServerConstants.PAGE_LIMIT)
				.offset(offSet).order("-returnedTime");
		
		QueryResultIterator<History> iterator = query.iterator();
		List<History> list = new ArrayList<History>();
		while (iterator.hasNext()) {
			History history = iterator.next();
			list.add(history);
		}
		Logger.getLogger("info").warning("getHistory list = " + list);

		return list;
	}
}
