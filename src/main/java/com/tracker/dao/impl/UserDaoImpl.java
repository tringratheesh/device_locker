package com.tracker.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.constants.ServerConstants;
import com.tracker.dao.UserDao;
import com.tracker.datastore.User;
import com.tracker.dto.LoginDto;
import com.tracker.util.ObjectifyUtil;

@Component
public class UserDaoImpl implements UserDao {

	@Autowired
	private ObjectifyUtil<User> objUtil;
		
	@Override
	public void createUser(LoginDto dto) {
		
		User user = new User();
		
//		user.setAuthId(dto.getAuthId());
		user.setEmail(dto.getEmail());
		user.setName(dto.getName());
		user.setImageUrl(dto.getImageUrl());
		user.setRole(ServerConstants.Role.user.name());
		user.setTermsAccepted(false);
		
		// Audit fields
		user.setCreatedBy(dto.getEmail());
		user.setCreatedTime(System.currentTimeMillis());
		user.setUpdatedBy(dto.getEmail());
		user.setUpdatedTime(System.currentTimeMillis());
		user.setDeleted(false);
		
		objUtil.save(user);
//		user = (User) objUtil.save(user);
		
//		return user;
	}

	@Override
	public User getUser(long userId) {

		return (User) objUtil.retrive(User.class, userId);
	}

	@Override
	public String getEmail(long userId) {
		
		User user = getUser(userId);
		return user.getEmail();
	}

	@Override
	public Boolean checkEmailExists(String email) {

		User user = (User) objUtil.retrive(User.class, "email", email);		
		return (user != null);
	}

	@Override
	public User getUserDetail(String email) {

		return (User) objUtil.retrive(User.class, "email", email);
	}

	@Override
	public List<User> getAdminUsers() {
		// TODO Auto-generated method stub
		
		List<Object> list = objUtil.retriveList(User.class, "role", ServerConstants.Role.admin);

		List<User> userList = new ArrayList<User>();
		
		for (Object obj : list) {
			
			User user = (User)obj;
			userList.add(user);
		}
		return userList;
	}

	@Override
	public void updateTermsCondition(User user) {
		
		user.setTermsAccepted(true);

		// Audit fields
		user.setUpdatedBy(user.getEmail());
		user.setUpdatedTime(System.currentTimeMillis());

		objUtil.save(user);

	}
	
}
