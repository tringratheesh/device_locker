package com.tracker.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.dao.PlatformDao;
import com.tracker.datastore.Platform;
import com.tracker.dto.PlatformDeleteDto;
import com.tracker.dto.PlatformDto;
import com.tracker.dto.PlatformUpdateDto;
import com.tracker.util.ObjectifyUtil;

@Component
public class PlatformDaoImpl implements PlatformDao {

	@Autowired
	private ObjectifyUtil<Platform> objUtil;

	@Override
	public void createPlatform(PlatformDto dto, String email) {

		Platform platform = new Platform();
		platform.setName(dto.getName());
		platform.setLogo(dto.getLogo());
		
		// Audit fields
		platform.setCreatedBy(email);
		platform.setCreatedTime(System.currentTimeMillis());
		platform.setUpdatedBy(email);
		platform.setUpdatedTime(System.currentTimeMillis());
		platform.setDeleted(false);

		objUtil.save(platform);

	}

	@Override
	public List<PlatformUpdateDto> getPlatforms() {

		List<Object> list = objUtil.retriveList(Platform.class, "isDeleted", false, "- createdTime");

		List<PlatformUpdateDto> platforms = new ArrayList<PlatformUpdateDto>();

		for (Object obj : list) {

			PlatformUpdateDto dto = new PlatformUpdateDto();
			Platform platform = (Platform) obj;

			dto.setName(platform.getName());
			dto.setLogo(platform.getLogo());
			dto.setPlatformId(platform.getPlatformId());
			platforms.add(dto);
		}
		return platforms;
	}

	@Override
	public Boolean updatePlatform(PlatformUpdateDto dto, String email) {

		Platform platform = (Platform) objUtil.retrive(Platform.class, dto.getPlatformId());

		if (platform != null) {
			platform.setName(dto.getName());
			platform.setLogo(dto.getLogo());

			// Audit fields
			platform.setUpdatedBy(email);
			platform.setUpdatedTime(System.currentTimeMillis());
			objUtil.save(platform);

			return true;
		}
		return false;
	}

	@Override
	public Boolean deletePlatform(PlatformDeleteDto dto, String email) {

		Platform platform = (Platform) objUtil.retrive(Platform.class, dto.getPlatformId());

		if (platform != null) {
			platform.setDeleted(true);

			// Audit fields
			platform.setUpdatedBy(email);
			platform.setUpdatedTime(System.currentTimeMillis());
			objUtil.save(platform);

			return true;
		}
		return false;
	}

	@Override
	public Platform getPlatform(long platformId) {

		return (Platform) objUtil.retrive(Platform.class, platformId);
	}

	@Override
	public Platform findPlatform(String key, String value) {

		return (Platform) objUtil.retrive(Platform.class, key, value);
	}

}
