package com.tracker.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.dao.ProjectDao;
import com.tracker.datastore.Project;
import com.tracker.dto.ProjectDeleteDto;
import com.tracker.dto.ProjectDto;
import com.tracker.dto.ProjectUpdateDto;
import com.tracker.dto.response.ProjectDetail;
import com.tracker.util.ObjectifyUtil;

@Component
public class ProjectDaoImpl implements ProjectDao {

	@Autowired
	private ObjectifyUtil<Project> objUtil;
		
	@Override
	public void createProject(ProjectDto dto, String email) {

		Project project = new Project();
		project.setName(dto.getName());

		// Audit fields
		project.setCreatedBy(email);
		project.setCreatedTime(System.currentTimeMillis());
		project.setUpdatedBy(email);
		project.setUpdatedTime(System.currentTimeMillis());
		project.setDeleted(false);

		objUtil.save(project);
	}

	@Override
	public List<ProjectDetail> getProjects() {

		List<Object> list = objUtil.retriveList(Project.class, "isDeleted", false, "- createdTime");

		List<ProjectDetail> projects = new ArrayList<ProjectDetail>();

		for (Object obj : list) {
			
			ProjectDetail detail = new ProjectDetail();
			Project project = (Project) obj;
			detail.setName(project.getName());
			detail.setProjectId(project.getProjectId());
			detail.setCreatedTime(project.getCreatedTime());
			projects.add(detail);
		}
		return projects;
	}

	@Override
	public Boolean updateProject(ProjectUpdateDto dto, String email) {

		Project project = (Project) objUtil.retrive(Project.class, dto.getProjectId());

		if (project != null) {
			project.setName(dto.getName());

			// Audit fields
			project.setUpdatedBy(email);
			project.setUpdatedTime(System.currentTimeMillis());
			objUtil.save(project);
			
			return true;
		}
		return false;
	}

	@Override
	public Boolean deleteProject(ProjectDeleteDto dto, String email) {

		Project project = (Project) objUtil.retrive(Project.class, dto.getProjectId());

		if (project != null) {
			project.setDeleted(true);

			// Audit fields
			project.setUpdatedBy(email);
			project.setUpdatedTime(System.currentTimeMillis());
			objUtil.save(project);
			
			return true;
		}
		return false;
	}

	@Override
	public Project getProject(long projectId) {

		return (Project) objUtil.retrive(Project.class, projectId);
	}

	@Override
	public Project findProject(String key, String value) {

		return (Project) objUtil.retrive(Project.class, key, value);
	}

}
