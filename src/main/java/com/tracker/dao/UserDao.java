package com.tracker.dao;

import java.util.List;

import com.tracker.datastore.User;
import com.tracker.dto.LoginDto;

public interface UserDao {

	void createUser(LoginDto dto);
	
	User getUser(long userId);
	
	String getEmail(long userId);
	
	User getUserDetail(String email);

	Boolean checkEmailExists(String email);
	
	List<User> getAdminUsers();
	
	void updateTermsCondition(User user);
}
