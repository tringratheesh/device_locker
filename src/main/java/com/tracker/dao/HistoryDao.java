package com.tracker.dao;

import java.util.List;

import com.googlecode.objectify.Objectify;
import com.tracker.datastore.History;

public interface HistoryDao {

	void createHistory(Objectify ofy, long userId, long deviceId, long projectId, long bookedTime);
			
//	List<History> getHistory(String pageNum1);
	
	List<History> getPageHistory(int pageNum, long deviceId);

	int getAllHistoriesCount(long deviceId);
}
