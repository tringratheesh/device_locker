package com.tracker.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tracker.dto.DeviceDto;
import com.tracker.dto.DeviceReturnDto;
import com.tracker.dto.LockerDto;
import com.tracker.dto.response.BookedDeviceResponse;
import com.tracker.dto.response.DeviceResponse;
import com.tracker.dto.response.DeviceResponseDto;
import com.tracker.dto.response.HistoryResponse;
import com.tracker.dto.response.Response;
import com.tracker.exception.InvalidException;
import com.tracker.handler.DeviceHandler;
import com.tracker.validator.DeviceValidator;

@RestController
@RequestMapping("device")
public class DeviceEndpoint {

	@Autowired
	private DeviceHandler handler;
	
	@Autowired
	private DeviceValidator validator;
	
	@RequestMapping(value = "register", method = RequestMethod.POST)
	public Response createDevice(@RequestBody DeviceDto deviceDetails) throws InvalidException {

		validator.validateCreateDevice(deviceDetails);

		return handler.createDevice(deviceDetails);
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public Response updateDevice(@RequestBody DeviceResponseDto deviceDetails) throws InvalidException {

		validator.validateUpdateDevice(deviceDetails);

		return handler.updateDevice(deviceDetails);
	}

	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public Response deleteDevice(@RequestBody DeviceReturnDto deviceDetails) throws InvalidException {

		validator.validateDeleteDevice(deviceDetails);
		
		return handler.deleteDevice(deviceDetails);
	}
		
	@RequestMapping(value = "book", method = RequestMethod.POST)
	public Response createLocker(@RequestBody LockerDto lockerDetails) throws InvalidException {
		
		validator.validateLocker(lockerDetails);

		return handler.createLocker(lockerDetails);
	}
	
	@RequestMapping(value = "return", method = RequestMethod.POST)
	public Response returnLocker(@RequestBody DeviceReturnDto deviceDetails) throws InvalidException {
		
		validator.validateDeletelocker(deviceDetails);

		return handler.deleteLocker(deviceDetails);
	}

	@RequestMapping(value = "list/booked", method = RequestMethod.GET)
	public BookedDeviceResponse getBookedDevices() {
		
		return handler.getOccupiedDevices();
	}
	
	@RequestMapping(value = "list/available", method = RequestMethod.GET)
	public DeviceResponse getAvailableDevices() {
		
		return handler.getAvailableDevices();
	}

	@RequestMapping(value = "history/{device_id}/page/{page_num}", method = RequestMethod.GET)
	public HistoryResponse getHistory(@PathVariable("device_id") long deviceId, @PathVariable("page_num") int pageNum) throws InvalidException {

		return handler.getHistory(pageNum, deviceId);
	}

}
