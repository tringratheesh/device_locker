package com.tracker.endpoints;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tracker.dto.LoginDto;
import com.tracker.dto.response.LoginResponse;
import com.tracker.dto.response.Response;
import com.tracker.exception.InvalidException;
import com.tracker.handler.UserHandler;
import com.tracker.validator.UserValidator;

@RestController
@RequestMapping("user")
public class UserEndpoint {

	@Autowired
	private UserHandler handler;

	/**
	 * Login API Call
	 * 
	 * @param loginDetails
	 * @return LoginResponse
	 * @throws InvalidException
	 */
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public LoginResponse login(@RequestBody LoginDto loginDetails) throws InvalidException {

		UserValidator.validateLogin(loginDetails);
		Logger.getLogger("info").warning("Google Id Token  = " + loginDetails.getTokenId());
		return handler.login(loginDetails);
	}

	/**
	 * Logout API Call
	 * 
	 * @return Response
	 * @throws InvalidException
	 */
	@RequestMapping(value = "logout", method = RequestMethod.POST)
	public Response logout() throws InvalidException {

		return handler.logout();
	}

	/**
	 * Accept terms and condition
	 * @return Response
	 * @throws InvalidException
	 */
	
	@RequestMapping(value = "terms/accept", method = RequestMethod.GET)
	public Response updateTerms() throws InvalidException {

		return handler.updateTerms();
	}

	/*
	 * @RequestMapping(value = "test", method = RequestMethod.GET) public String
	 * test() {
	 * 
	 * System.out.println("str == "+ ServerConstants.Role.admin); return
	 * ServerConstants.Role.admin.name(); }
	 */
	
	@RequestMapping(value = "login/test", method = RequestMethod.POST)
	public LoginResponse testLogin(@RequestBody LoginDto loginDetails) throws InvalidException {

		return handler.testLogin(loginDetails);
	}
}
