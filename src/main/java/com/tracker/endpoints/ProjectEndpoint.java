package com.tracker.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tracker.dto.ProjectDeleteDto;
import com.tracker.dto.ProjectDto;
import com.tracker.dto.ProjectUpdateDto;
import com.tracker.dto.response.ProjectResponse;
import com.tracker.dto.response.Response;
import com.tracker.exception.InvalidException;
import com.tracker.handler.ProjectHandler;
import com.tracker.validator.ProjectValidator;

@RestController
@RequestMapping("project")
public class ProjectEndpoint {

	@Autowired
	private ProjectHandler handler;

	@Autowired
	private ProjectValidator validator;
	
	@RequestMapping(value = "register", method = RequestMethod.POST)
	public Response createProject(@RequestBody ProjectDto projectDetails) throws InvalidException {

		validator.validateCreateProject(projectDetails);

		return handler.createProject(projectDetails);
	}

	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ProjectResponse projectList() {

		return handler.getProjects();
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public Response updateProject(@RequestBody ProjectUpdateDto projectDetails) throws InvalidException {

		validator.validateUpdateProject(projectDetails);

		return handler.updateProject(projectDetails);
	}

	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public Response deleteProject(@RequestBody ProjectDeleteDto projectDetails) throws InvalidException {

		validator.validateDeleteProject(projectDetails);

		return handler.deleteProject(projectDetails);
	}

}
