package com.tracker.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tracker.dto.PlatformDeleteDto;
import com.tracker.dto.PlatformDto;
import com.tracker.dto.PlatformUpdateDto;
import com.tracker.dto.response.PlatformResponse;
import com.tracker.dto.response.Response;
import com.tracker.exception.InvalidException;
import com.tracker.handler.PlatformHandler;
import com.tracker.util.SGMailHandler;
import com.tracker.validator.PlatformValidator;

@RestController
@RequestMapping("platform")
public class PlatformEndpoint {

	@Autowired
	private PlatformHandler handler;

	@Autowired
	private PlatformValidator validator;
	
	@RequestMapping(value = "test/email", method = RequestMethod.GET)
	public void sendEmail()  {

		SGMailHandler mailHandler = new SGMailHandler();
		
		String body = "<html>\n" + 
				"    <head>\n" + 
				"        <title>Template</title>\n" + 
				"    </head>\n" + 
				"    <body style='background: #F7F7F7;'>\n" + 
				"        <div style='text-align: center'>\n" + 
				"            <h2 style='font-family: sans-serif; padding:30px; display: inline-block;color:#FF3B30; width : 65%; '>Your leave Request has been " + "cancelled"+"</h2>    \n"+
				"            <div style='height: 60%; width : 65%; background: #FFFFFF; display: inline-block;'>\n" + 
				"                <table style='font-family: sans-serif; color: #9B9AB2; display: inline-block; margin-top: 50px'>\n" + 
				"                    <tr><th style='text-align: right; padding: 10px'>Type</th><td style='text-align: left; padding-left: 70px'> CL</td></tr>\n"+""+
				"                    <tr><th style='text-align: right; padding: 10px'>Start Date</th><td style='text-align: left; padding-left: 70px'>jhnfgj</td></tr>\n"+
				"                    <tr><th style='text-align: right; padding: 10px'>End Date</th><td style='text-align: left; padding-left: 70px'> 23/3/324</td></tr>\n"+
				"                    <tr><th style='text-align: right; padding: 10px'>Reason For Applying</th><td style='text-align: left; padding-left: 70px'>don't kdcjd f dls fgkldfgl</td></tr>\n"+
				"                </table>\n" + 
				"            </div>\n" + 
				"        </div>\n" + 
				"    </body>\n" + 
				"</html>";
		
		mailHandler.send("ratheesh.r@tringapps.com", "Device Tracker", body);
		
	}

	@RequestMapping(value = "register", method = RequestMethod.POST)
	public Response createPlatform(@RequestBody PlatformDto platformDetails) throws InvalidException {

		validator.validateCreatePlatform(platformDetails);

		return handler.createPlaform(platformDetails);
	}

	@RequestMapping(value = "list", method = RequestMethod.GET)
	public PlatformResponse projectList() {

		return handler.getPlatforms();
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public Response updatePlatform(@RequestBody PlatformUpdateDto platformDetails) throws InvalidException {

		validator.validateUpdatePlatform(platformDetails);

		return handler.updatePlatform(platformDetails);
	}

	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public Response deletePlatform(@RequestBody PlatformDeleteDto platformDetails) throws InvalidException {

		validator.validateDeletePlatform(platformDetails);

		return handler.deletePlatform(platformDetails);
	}

}
