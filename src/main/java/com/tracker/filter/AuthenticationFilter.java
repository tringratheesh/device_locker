package com.tracker.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.google.appengine.api.NamespaceManager;
import com.tracker.constants.ServerConstants;

@WebFilter(displayName = "AuthenticationFilter", urlPatterns = { "/*" })
public class AuthenticationFilter implements javax.servlet.Filter {

	private static Logger log = Logger.getLogger(AuthenticationFilter.class.getName());

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, filterConfig.getServletContext());
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		String methodType = httpServletRequest.getMethod();
		String headerOrigin = httpServletRequest.getHeader("origin");

		httpServletResponse.setHeader("Access-Control-Allow-Origin", headerOrigin);
		httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");
		httpServletResponse.setHeader("Access-Control-Request-Methods", "GET, POST, PUT, DELETE,OPTIONS");
		httpServletResponse.setHeader("Origin", headerOrigin);
		httpServletResponse.setHeader("Content-Type", "application/json");
		httpServletResponse.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type,Credential,content-type,Accept,*,header-origin,Expires");

		if (methodType.equalsIgnoreCase("options")) {

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			chain.doFilter(httpServletRequest, httpServletResponse);

			return;
		}

		if (NamespaceManager.get() == null) {
			log.info("Storage:: " + ServerConstants.STORAGE_NAME);
			NamespaceManager.set(ServerConstants.STORAGE_NAME);
		}

		if (request instanceof HttpServletRequest) {

			HttpSession session = httpServletRequest.getSession();
			Logger.getLogger("info").warning("Session id = " + session.getId());
			Logger.getLogger("info")
					.warning("Session user id = " + session.getAttribute(ServerConstants.SessionKey.userId.name()));
			Logger.getLogger("info")
					.warning("Session role = " + session.getAttribute(ServerConstants.SessionKey.role.name()));
			Logger.getLogger("info").warning(
					"Session createdTime = " + session.getAttribute(ServerConstants.SessionKey.createdTime.name()));

			String reqUrl = httpServletRequest.getRequestURL().toString();

			if (validUrls(reqUrl) || authenticate(session, reqUrl)) {

				chain.doFilter(request, response);
			} else {
				Logger.getLogger("info").warning("reqUrl = " + reqUrl + " Not Valid");
				httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}
		}

	}

	private boolean validUrls(String url) {
		return url.equals("http://localhost:8080/device-tracker/") || url.contains("swagger") || url.contains("login")
				|| url.contains("signup") || url.contains("api-docs") || url.contains("_ah/admin")
				|| url.contains("test") || url.contains("remindDevice") || url.contains("dailyReport");
	}

	private boolean authenticate(HttpSession session, String url) {

		if (session == null || session.getAttribute(ServerConstants.SessionKey.userId.name()) == null
				|| session.getAttribute(ServerConstants.SessionKey.role.name()) == null
				|| session.getAttribute(ServerConstants.SessionKey.createdTime.name()) == null)
			return false;

		try {
			long sessionCreatedTime = (long) session.getAttribute(ServerConstants.SessionKey.createdTime.name());
			long expiryInterval = sessionCreatedTime + ServerConstants.SESSION_EXPIRY_INTERVAL;
			Logger.getLogger("info").warning("Session expiryInterval = " + expiryInterval);

			if (System.currentTimeMillis() < expiryInterval) {

				for (String adminEndpoint : ServerConstants.ADMIN_ENDPOINTS) {
					if (url.contains(adminEndpoint)) {
						return ServerConstants.Role.admin.name()
								.equals(session.getAttribute(ServerConstants.SessionKey.role.name()));
					}
				}
				return true;
			}
			Logger.getLogger("info").warning("Login timestamp is not valid");

			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
