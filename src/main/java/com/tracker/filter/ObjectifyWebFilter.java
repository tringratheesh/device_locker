package com.tracker.filter;

import javax.servlet.annotation.WebFilter;

import com.googlecode.objectify.ObjectifyFilter;

@WebFilter(displayName="ObjectifyWebFilter",urlPatterns = {"/*"})
public class ObjectifyWebFilter extends ObjectifyFilter {

}
