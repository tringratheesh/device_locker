package com.tracker.datastore;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Index
@Entity
public class Locker implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long lockerId;
	
	private long deviceId;
	
	private long projectId;
	
	private long userId;

	private long updatedTime;
	
	public long getLockerId() {
		return lockerId;
	}

	public void setLockerId(long lockerId) {
		this.lockerId = lockerId;
	}

	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}
	
}
