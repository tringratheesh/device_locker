package com.tracker.datastore;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Index
@Entity
public class History implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long historyId;
	
	private long deviceId;
	
	private long userId;
	
	private long projectId;
		
	private long bookedTime;
	
	private long returnedTime;
	
	private long createdTime;

	public long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(long historyId) {
		this.historyId = historyId;
	}

	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getBookedTime() {
		return bookedTime;
	}

	public void setBookedTime(long bookedTime) {
		this.bookedTime = bookedTime;
	}

	public long getReturnedTime() {
		return returnedTime;
	}

	public void setReturnedTime(long returnedTime) {
		this.returnedTime = returnedTime;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}
}
