package com.tracker.validator;

import java.util.regex.Matcher;

import com.tracker.constants.ServerConstants;

public class Validator {

	public static boolean isValidEmail(String str) {
		
		Matcher matcher = ServerConstants.EMAIL_ADDRESS_REGEX.matcher(str);
		return matcher.find();
	}
	
	public static boolean isValid(String str) {
		return str != null && !str.trim().isEmpty();
	}

}
