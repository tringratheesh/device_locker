package com.tracker.validator;

import java.util.Collections;
import java.util.logging.Logger;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.tracker.constants.ErrorConstants;
import com.tracker.constants.ServerConstants;
import com.tracker.exception.InvalidException;

public class GoogleTokenVerifier {

	public static String verifyGoogleToken(String authId) throws InvalidException {

		try {
			final HttpTransport TRANSPORT = new NetHttpTransport();
			final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

			GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(TRANSPORT, JSON_FACTORY)
					.setAudience(Collections.singletonList(ServerConstants.CLIENT_ID)).build();

			GoogleIdToken token = verifier.verify(authId);

			if (token != null) {

				Payload payload = token.getPayload();
				
				return payload.getEmail();
			} else {
				Logger.getLogger("info").warning("Invalid google token");
				throw new InvalidException(ErrorConstants.DT_ERR_006, ErrorConstants.DT_ERR_MSG_006);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidException(ErrorConstants.DT_ERR_018, ErrorConstants.DT_ERR_MSG_018);
		}

	}
}
