package com.tracker.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.constants.ErrorConstants;
import com.tracker.dao.PlatformDao;
import com.tracker.dto.PlatformDeleteDto;
import com.tracker.dto.PlatformDto;
import com.tracker.dto.PlatformUpdateDto;
import com.tracker.exception.InvalidException;

@Component
public class PlatformValidator extends Validator {

	@Autowired
	private PlatformDao dao;

	private final String PLATFORM_NAME_KEY = "name";
	
	public void validateCreatePlatform(PlatformDto dto) throws InvalidException {
		
		validateName(dto.getName());
		validateStoredName(dto.getName());
	}
	
	public void validateUpdatePlatform(PlatformUpdateDto dto) throws InvalidException {

		validateName(dto.getName());
		validateId(dto.getPlatformId());
	}
	
	public void validateDeletePlatform(PlatformDeleteDto dto) throws InvalidException {

		validateId(dto.getPlatformId());
	}

	/*
	 *  In build methods
	 */
	
	private void validateId(long id) throws InvalidException {
		
		if (id == 0) {
			throw new InvalidException(ErrorConstants.DT_ERR_023, ErrorConstants.DT_ERR_MSG_023);
		}
	}
	
	private void validateName(String name) throws InvalidException {

		if (!isValid(name)) {
			throw new InvalidException(ErrorConstants.DT_ERR_022, ErrorConstants.DT_ERR_MSG_022);
		}	
	}
	
	private void validateStoredName(String name) throws InvalidException {
		
		if (dao.findPlatform(PLATFORM_NAME_KEY, name) != null) {
			throw new InvalidException(ErrorConstants.DT_ERR_024, ErrorConstants.DT_ERR_MSG_024);
		}
	}
	
}
