package com.tracker.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.constants.ErrorConstants;
import com.tracker.dao.DeviceDao;
import com.tracker.dao.PlatformDao;
import com.tracker.dto.DeviceDto;
import com.tracker.dto.DeviceReturnDto;
import com.tracker.dto.LockerDto;
import com.tracker.dto.response.DeviceResponseDto;
import com.tracker.exception.InvalidException;

@Component
public class DeviceValidator extends Validator {

	@Autowired
	private DeviceDao dao;
	
	@Autowired
	private PlatformDao platformDao;
	
	private final String NICK_NAME_KEY = "nickName";
	private final String IP_ADDRESS_KEY = "ipAddress";
	private final String MAC_ADDRESS_KEY = "macAddress";
	
	public void validateCreateDevice(DeviceDto dto) throws InvalidException {

		validateName(dto.getNickName());
		validateStoredName(dto.getNickName());
		validatePlatformId(dto.getPlatformId());
		
		validateIpAddress(dto.getIpAddress());
		validateMacAddress(dto.getMacAddress());
		validateStoredIpAddress(dto.getIpAddress());
		validateStoredMacAddress(dto.getMacAddress());

		validateOS(dto.getOsVersion());
		validateCapacity(dto.getCapacity());
		validateColour(dto.getColor());
		validateModel(dto.getName());
	}

	public void validateUpdateDevice(DeviceResponseDto dto) throws InvalidException {

		validateDeviceId(dto.getDeviceId());
		validateName(dto.getNickName());
		validatePlatformId(dto.getPlatformId());
	}

	public void validateDeleteDevice(DeviceReturnDto dto) throws InvalidException {

		validateDeviceId(dto.getDeviceId());
	}

	public void validateLocker(LockerDto dto) throws InvalidException {
		
		validateDeviceId(dto.getDeviceId());
		validateProjectId(dto.getProjectId());
	}
	
	public void validateDeletelocker(DeviceReturnDto dto) throws InvalidException {
		
		validateDeviceId(dto.getDeviceId());
	}

	/*
	 *  In build methods
	 */
	
	private void validateDeviceId(long deviceId) throws InvalidException {
		
		if (deviceId == 0) {
			throw new InvalidException(ErrorConstants.DT_ERR_009, ErrorConstants.DT_ERR_MSG_009);
		}
	}
	
	private void validateProjectId(long projectId) throws InvalidException {
		
		if (projectId == 0) {
			throw new InvalidException(ErrorConstants.DT_ERR_011, ErrorConstants.DT_ERR_MSG_011);
		}
	}
	
	private void validateName(String name) throws InvalidException {
		
		if (!isValid(name)) {
			throw new InvalidException(ErrorConstants.DT_ERR_008, ErrorConstants.DT_ERR_MSG_008);
		}
	}
	
	private void validateStoredName(String name) throws InvalidException {
		
		if (dao.findDevice(NICK_NAME_KEY, name) != null) {
			throw new InvalidException(ErrorConstants.DT_ERR_026, ErrorConstants.DT_ERR_MSG_026);
		}
	}
	
	private void validateStoredIpAddress(String address) throws InvalidException {
		
		if (dao.findDevice(IP_ADDRESS_KEY, address) != null) {
			throw new InvalidException(ErrorConstants.DT_ERR_027, ErrorConstants.DT_ERR_MSG_027);
		}
	}

	private void validateStoredMacAddress(String address) throws InvalidException {
		
		if (dao.findDevice(MAC_ADDRESS_KEY, address) != null) {
			throw new InvalidException(ErrorConstants.DT_ERR_028, ErrorConstants.DT_ERR_MSG_028);
		}
	}
	
	private void validatePlatformId(long platformId) throws InvalidException {
		
		if (platformId == 0) {
			throw new InvalidException(ErrorConstants.DT_ERR_023, ErrorConstants.DT_ERR_MSG_023);
		}

		if (platformDao.getPlatform(platformId) == null) {
			throw new InvalidException(ErrorConstants.DT_ERR_029, ErrorConstants.DT_ERR_MSG_029);
		}
	}

	private void validateIpAddress(String ipAddress) throws InvalidException {
		
		if (!isValid(ipAddress)) {
			throw new InvalidException(ErrorConstants.DT_ERR_030, ErrorConstants.DT_ERR_MSG_030);
		}
	}
	
	private void validateMacAddress(String macAddress) throws InvalidException {
		
		if (!isValid(macAddress)) {
			throw new InvalidException(ErrorConstants.DT_ERR_031, ErrorConstants.DT_ERR_MSG_031);
		}
	}

	private void validateOS(String os) throws InvalidException {
		
		if (!isValid(os)) {
			throw new InvalidException(ErrorConstants.DT_ERR_032, ErrorConstants.DT_ERR_MSG_032);
		}
	}

	private void validateCapacity(int capacity) throws InvalidException {
		
		if (capacity == 0) {
			throw new InvalidException(ErrorConstants.DT_ERR_033, ErrorConstants.DT_ERR_MSG_033);
		}
	}

	private void validateColour(String colour) throws InvalidException {
		
		if (!isValid(colour)) {
			throw new InvalidException(ErrorConstants.DT_ERR_034, ErrorConstants.DT_ERR_MSG_034);
		}
	}

	private void validateModel(String model) throws InvalidException {
		
		if (!isValid(model)) {
			throw new InvalidException(ErrorConstants.DT_ERR_035, ErrorConstants.DT_ERR_MSG_035);
		}
	}

}
