package com.tracker.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.constants.ErrorConstants;
import com.tracker.dao.ProjectDao;
import com.tracker.dto.ProjectDeleteDto;
import com.tracker.dto.ProjectDto;
import com.tracker.dto.ProjectUpdateDto;
import com.tracker.exception.InvalidException;

@Component
public class ProjectValidator extends Validator {

	@Autowired
	private ProjectDao dao;
	
	private final String PROJECT_NAME_KEY = "name";

	public void validateCreateProject(ProjectDto dto) throws InvalidException {
		
		validateName(dto.getName());
		validateStoredName(dto.getName());
	}
	
	public void validateUpdateProject(ProjectUpdateDto dto) throws InvalidException {

		validateId(dto.getProjectId());
		validateName(dto.getName());
	}
	
	public void validateDeleteProject(ProjectDeleteDto dto) throws InvalidException {
		
		validateId(dto.getProjectId());
	}
	
	/*
	 *  In build methods
	 */
	
	private void validateId(long id) throws InvalidException {
		
		if (id == 0) {
			throw new InvalidException(ErrorConstants.DT_ERR_011, ErrorConstants.DT_ERR_MSG_011);
		}
	}
	
	private void validateName(String name) throws InvalidException {

		if (!isValid(name)) {
			throw new InvalidException(ErrorConstants.DT_ERR_010, ErrorConstants.DT_ERR_MSG_010);
		}	
	}
	
	private void validateStoredName(String name) throws InvalidException {
		
		if (dao.findProject(PROJECT_NAME_KEY, name) != null) {
			throw new InvalidException(ErrorConstants.DT_ERR_025, ErrorConstants.DT_ERR_MSG_025);
		}
	}

}
