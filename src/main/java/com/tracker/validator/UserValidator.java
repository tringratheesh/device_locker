package com.tracker.validator;

import com.tracker.constants.ErrorConstants;
import com.tracker.constants.ServerConstants;
import com.tracker.dto.LoginDto;
import com.tracker.exception.InvalidException;

public class UserValidator extends Validator{

	public static void validateLogin(LoginDto dto) throws InvalidException {
		
		if (!isValid(dto.getTokenId())) {
			throw new InvalidException(ErrorConstants.DT_ERR_001, ErrorConstants.DT_ERR_MSG_001);
		}
		if (!isValid(dto.getEmail())) {
			throw new InvalidException(ErrorConstants.DT_ERR_002, ErrorConstants.DT_ERR_MSG_002);
		}
		if (!isValid(dto.getName())) {
			throw new InvalidException(ErrorConstants.DT_ERR_003, ErrorConstants.DT_ERR_MSG_003);
		}
		if (!isValidEmail(dto.getEmail())) {
			throw new InvalidException(ErrorConstants.DT_ERR_004, ErrorConstants.DT_ERR_MSG_004);
		}
		if(!dto.getEmail().contains(ServerConstants.VALID_EMAIL)) {
			throw new InvalidException(ErrorConstants.DT_ERR_005, ErrorConstants.DT_ERR_MSG_005);
		}
	}

}
