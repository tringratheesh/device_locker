package com.tracker.dto;

import java.io.Serializable;

public class PlatformDeleteDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long platformId;

	public long getPlatformId() {
		return platformId;
	}

	public void setPlatformId(long platformId) {
		this.platformId = platformId;
	}
	
}
