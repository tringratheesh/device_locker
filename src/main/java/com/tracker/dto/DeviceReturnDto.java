package com.tracker.dto;

import java.io.Serializable;

public class DeviceReturnDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long deviceId;

	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}

}
