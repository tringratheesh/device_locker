package com.tracker.dto;

import java.io.Serializable;

public class UserDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long userId;
	private String userName;
	private String email;
//	private String authId;
	private String imageUrl;
	private String role;
	private boolean isTermsAccepted;

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
//	public String getAuthId() {
//		return authId;
//	}
//	public void setAuthId(String authId) {
//		this.authId = authId;
//	}
	public String getRole() {
		return role;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public boolean getIsTermsAccepted() {
		return isTermsAccepted;
	}
	public void setIsTermsAccepted(boolean isTermsAccepted) {
		this.isTermsAccepted = isTermsAccepted;
	}
	
}
