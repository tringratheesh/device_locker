package com.tracker.dto;

public class ProjectUpdateDto extends ProjectDto {

	private static final long serialVersionUID = 1L;
	
	private long projectId;

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}	
}
