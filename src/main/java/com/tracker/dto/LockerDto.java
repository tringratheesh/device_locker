package com.tracker.dto;

import java.io.Serializable;

public class LockerDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long deviceId;
	private long projectId;
	
	public long getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
}
