package com.tracker.dto;

import com.tracker.dto.response.DeviceResponseDto;

public class BookedDetails extends DeviceResponseDto {

	private static final long serialVersionUID = 1L;
	
	private long userId;
	private String userName;
	
	private long projectId;
	private String projectName;
		
	private long lockerId;
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	} 

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public long getLockerId() {
		return lockerId;
	}

	public void setLockerId(long lockerId) {
		this.lockerId = lockerId;
	}
}
