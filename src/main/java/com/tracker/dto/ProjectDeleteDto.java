package com.tracker.dto;

import java.io.Serializable;

public class ProjectDeleteDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long projectId;

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	
}
