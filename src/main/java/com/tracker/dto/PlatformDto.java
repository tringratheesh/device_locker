package com.tracker.dto;

import java.io.Serializable;

public class PlatformDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	private String logo;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
	
}
