package com.tracker.dto.response;

public class HistoryDetails {

	private long userId;
	private String userName;
	private String projectName;
	private long historyId;
	private long bookedTime;
	private long returnedTime;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(long historyId) {
		this.historyId = historyId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public long getBookedTime() {
		return bookedTime;
	}

	public void setBookedTime(long bookedTime) {
		this.bookedTime = bookedTime;
	}

	public long getReturnedTime() {
		return returnedTime;
	}

	public void setReturnedTime(long returnedTime) {
		this.returnedTime = returnedTime;
	}

}
