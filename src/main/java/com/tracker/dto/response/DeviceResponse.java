package com.tracker.dto.response;

import java.util.List;

public class DeviceResponse extends Response {

	public DeviceResponse(String status, String code, String message) {
		super(status, code, message);
	}

	private static final long serialVersionUID = 1L;
	
	private List<DeviceResponseDto> data;

	public List<DeviceResponseDto> getData() {
		return data;
	}

	public void setData(List<DeviceResponseDto> data) {
		this.data = data;
	}	
}
