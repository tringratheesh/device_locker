package com.tracker.dto.response;

import com.tracker.dto.ProjectUpdateDto;

public class ProjectDetail extends ProjectUpdateDto {
	
	private static final long serialVersionUID = 1L;
	
	private long createdTime;

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}
}
