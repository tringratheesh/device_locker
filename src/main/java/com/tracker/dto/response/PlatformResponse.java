package com.tracker.dto.response;

import java.util.List;

import com.tracker.dto.PlatformUpdateDto;

public class PlatformResponse extends Response{

	private static final long serialVersionUID = 1L;

	public PlatformResponse(String status, String code, String message) {
		super(status, code, message);
		// TODO Auto-generated constructor stub
	}

	private List<PlatformUpdateDto> data;

	public List<PlatformUpdateDto> getData() {
		return data;
	}

	public void setData(List<PlatformUpdateDto> data) {
		this.data = data;
	}
	
}
