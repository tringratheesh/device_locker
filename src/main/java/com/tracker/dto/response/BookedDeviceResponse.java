package com.tracker.dto.response;

import java.util.List;

import com.tracker.dto.BookedDetails;

public class BookedDeviceResponse extends Response {


	public BookedDeviceResponse(String status, String code, String message) {
		super(status, code, message);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private List<BookedDetails> data;

	public List<BookedDetails> getData() {
		return data;
	}

	public void setData(List<BookedDetails> data) {
		this.data = data;
	}

}
