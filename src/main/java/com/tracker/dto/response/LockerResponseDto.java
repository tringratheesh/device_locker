package com.tracker.dto.response;

import com.tracker.dto.LockerDto;

public class LockerResponseDto extends LockerDto{

	private static final long serialVersionUID = 1L;
	
	private int lockerId;

	public int getLockerId() {
		return lockerId;
	}

	public void setLockerId(int lockerId) {
		this.lockerId = lockerId;
	}
	
}
	
	
