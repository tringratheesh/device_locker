package com.tracker.dto.response;

import com.tracker.dto.DeviceDto;

public class DeviceResponseDto extends DeviceDto {

	private static final long serialVersionUID = 1L;
	
	private long deviceId;
	private boolean isAvailable;
	
	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}
	
	public boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	
}
