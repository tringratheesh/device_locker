package com.tracker.dto.response;

import java.util.List;

public class HistoryResponse extends Response {

	private static final long serialVersionUID = 1L;
	
	public HistoryResponse(String status, String code, String message) {
		super(status, code, message);
		// TODO Auto-generated constructor stub
	}

	private List<HistoryDetails> data;
	private int totalCount;
	private int pageLimit;
	
	public List<HistoryDetails> getData() {
		return data;
	}

	public void setData(List<HistoryDetails> data) {
		this.data = data;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageLimit() {
		return pageLimit;
	}

	public void setPageLimit(int pageLimit) {
		this.pageLimit = pageLimit;
	}

}
