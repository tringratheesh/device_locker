package com.tracker.dto.response;

import java.util.List;

public class ProjectResponse extends Response  {

	public ProjectResponse(String status, String code, String message) {
		super(status, code, message);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private List<ProjectDetail> data;

	public List<ProjectDetail> getData() {
		return data;
	}

	public void setData(List<ProjectDetail> data) {
		this.data = data;
	}

}
