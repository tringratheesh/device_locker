package com.tracker.dto.response;

import com.tracker.dto.UserDetails;

public class LoginResponse extends Response {

	public LoginResponse(String status, String code, String message) {
		super(status, code, message);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private UserDetails data;

	public UserDetails getData() {
		return data;
	}

	public void setData(UserDetails data) {
		this.data = data;
	}
	
}
