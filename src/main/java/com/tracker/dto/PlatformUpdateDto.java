package com.tracker.dto;


public class PlatformUpdateDto extends PlatformDto{

	private static final long serialVersionUID = 1L;
	
	private long platformId;

	public long getPlatformId() {
		return platformId;
	}

	public void setPlatformId(long platformId) {
		this.platformId = platformId;
	}

}
