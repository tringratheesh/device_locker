package com.tracker.exception;

public class InvalidException extends Exception {

	private static final long serialVersionUID = 1L;

	private String status;
	private String code;
	private String message;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public InvalidException(String code, String message) {
		
		this.code = code;
		this.message = message;
	}
}
