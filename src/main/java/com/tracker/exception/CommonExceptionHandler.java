package com.tracker.exception;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.tracker.constants.ErrorConstants;
import com.tracker.dto.response.Response;

@RestController
@ControllerAdvice
public class CommonExceptionHandler {
	
	@ExceptionHandler(InvalidException.class)
	public Response exHandler(InvalidException e, HttpServletResponse resp) {
		
		Response response = new Response(ErrorConstants.ERROR, e.getCode(), e.getMessage());
		resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		e.printStackTrace();
		return response;
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	public Response exHandler(UnauthorizedException e, HttpServletResponse resp) {
		
		Response response = new Response(ErrorConstants.ERROR, e.getCode(), e.getMessage());
		resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		e.printStackTrace();
		return response;
	}

}
