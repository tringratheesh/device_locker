package com.tracker.util;

import java.io.IOException;
import java.util.logging.Logger;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import com.tracker.constants.ServerConstants;

public class SGMailHandler {

	  public void send(String toEmail, String subject, String body) {		    

		    Email toAddress = new Email(toEmail);
		    Email fromAddress = new Email(ServerConstants.SG_MAIL_SENDER);
		    
		    Content content = new Content("text/html", body);
		    
		    Mail mail = new Mail();
		    
		    mail.setFrom(fromAddress);
		    mail.addContent(content);
		    mail.setSubject(subject);
		      
		    Personalization person = new Personalization();
		    
		    person.addTo(toAddress);
		    mail.addPersonalization(person);
		    
		    // Instantiates SendGrid client.
		    SendGrid sendgrid = new SendGrid(ServerConstants.SG_API_KEY);

		    Logger.getLogger("info").warning("sendgrid key ="+sendgrid.getHost());
		    Logger.getLogger("info").warning("sendgrid key ="+sendgrid.getHost());

		    // Instantiate SendGrid request.
		    Request request = new Request();

		    try {
		      // Set request configuration.
		      request.setMethod(Method.POST);
		      request.setEndpoint("mail/send");
		      request.setBody(mail.build());		      

		      // Use the client to send the API request.
		      Response response = sendgrid.api(request);

		      if (response.getStatusCode() != 202) {
		        
		        return;
		      }

		     
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
		  }

}
