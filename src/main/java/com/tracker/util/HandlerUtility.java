package com.tracker.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.constants.ServerConstants;
import com.tracker.dao.UserDao;
import com.tracker.datastore.User;

@Component
public class HandlerUtility {

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserDao userDao;
	
	public User getUser() {

		long userId = (long)request.getSession().getAttribute(ServerConstants.SessionKey.userId.name());
		return userDao.getUser(userId);
	}

	public String getEmail() {

		long userId = (long)request.getSession().getAttribute("userId");
		return userDao.getEmail(userId);
	}

}
