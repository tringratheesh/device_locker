package com.tracker.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.googlecode.objectify.ObjectifyService;

@Component
public class ObjectifyUtil<T> {

	public static DatastoreService service = DatastoreServiceFactory.getDatastoreService();

	static {
		ObjectifyService.begin();
	}

	/*** Save and Update ***/
	
	public void save(T object) {

		ObjectifyService.ofy().save().entity(object).now();
	}

	public void saveBulk(List<T> list) {

		ObjectifyService.ofy().save().entities(list).now();
	}

	/*** Retrive  ***/

	public Object retrive(Class clazz, long id) {

		return ObjectifyService.ofy().load().type(clazz).id(id).now();
	}

	public Object retrive(Class clazz, String id) {

		return ObjectifyService.ofy().load().type(clazz).id(id).now();
	}

	public Object retrive(Class clazz, String property, Object value) {

		return ObjectifyService.ofy().load().type(clazz).filter(property, value).first().now();
	}

	public List<Object> retriveList(Class clazz) {

		return ObjectifyService.ofy().load().type(clazz).list();
	}
	
	public List<Object> retriveList(Class clazz, String orderBy) {

		return ObjectifyService.ofy().load().type(clazz).order(orderBy).list();
	}

	public List<Object> retriveList(Class clazz, String property, Object value) {

		return ObjectifyService.ofy().load().type(clazz).filter(property, value).list();
	}

	public List<Object> retriveList(Class clazz, String property, Object value, String orderBy) {

		return ObjectifyService.ofy().load().type(clazz).filter(property, value).order(orderBy).list();
	}

	public List<Object> retriveList(Class clazz, LinkedHashMap<String, Object> constraints, String orderBy) {

		Set<String> keys = constraints.keySet();
		ArrayList<Filter> arrayList = new ArrayList<Filter>();

		for (String key : keys) {
			arrayList.add(new FilterPredicate(key, FilterOperator.EQUAL, constraints.get(key)));
		}
		if(orderBy.length() > 0) 
			return ObjectifyService.ofy().load().type(clazz).filter(CompositeFilterOperator.and(arrayList)).order(orderBy).list();

		return ObjectifyService.ofy().load().type(clazz).filter(CompositeFilterOperator.and(arrayList)).list();
	}

	public Object retriveSingle(Class clazz, LinkedHashMap<String, Object> constraints) {
		
		List<Object> list = retriveList(clazz, constraints, "");
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	public Entity retrieveSingleEntity(String kind, String property, String value) {
		return service.prepare(new Query(kind).setFilter(new FilterPredicate(property, FilterOperator.EQUAL, value)))
				.asSingleEntity();
	}

	public Entity retrieveSingleEntity(String kind, long entryId) {
		return service.prepare(new Query(kind).setFilter(new FilterPredicate("entryId", FilterOperator.EQUAL, entryId)))
				.asSingleEntity();
	}

	public Entity retrieveSingleEntity(String kind, LinkedHashMap<String, String> constraints) {

		Set<String> keys = constraints.keySet();
		ArrayList<Filter> arrayList = new ArrayList<Filter>();

		for (String key : keys) {
			arrayList.add(new FilterPredicate(key, FilterOperator.EQUAL, constraints.get(key)));
		}
		return service.prepare(new Query(kind).setFilter(CompositeFilterOperator.and(arrayList))).asSingleEntity();
	}
	
	/*** Delete  ***/

	public void delete(Object object) {
		ObjectifyService.ofy().delete().entity(object).now();
	}

	public void delete(Entity entity) {
		service.delete(entity.getKey());
	}

}
