package com.tracker.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.constants.ErrorConstants;
import com.tracker.constants.ResponseConstants;
import com.tracker.constants.ServerConstants;
import com.tracker.dao.UserDao;
import com.tracker.datastore.User;
import com.tracker.dto.LoginDto;
import com.tracker.dto.UserDetails;
import com.tracker.dto.response.LoginResponse;
import com.tracker.dto.response.Response;
import com.tracker.exception.InvalidException;
import com.tracker.exception.UnauthorizedException;
import com.tracker.util.HandlerUtility;
import com.tracker.validator.GoogleTokenVerifier;

@Component
public class UserHandler {

	@Autowired
	private UserDao userDao;

	@Autowired
	private HandlerUtility handlerUtil;

	@Autowired
	private HttpServletRequest request;

	/**
	 * Login API call
	 * @param loginDetails
	 * @return LoginResponse
	 * @throws InvalidException
	 */
	public LoginResponse login(LoginDto loginDetails) throws InvalidException {

		String email = GoogleTokenVerifier.verifyGoogleToken(loginDetails.getTokenId());
		
		if (email != null && email.equals(loginDetails.getEmail())) 
		{
			User user = userDao.getUserDetail(loginDetails.getEmail());
			if (user == null) {
				userDao.createUser(loginDetails);
				user = userDao.getUserDetail(loginDetails.getEmail());
			}
			HttpSession session = request.getSession(true);
			session.setAttribute(ServerConstants.SessionKey.userId.name(), user.getUserId());
			session.setAttribute(ServerConstants.SessionKey.role.name(), user.getRole());
			session.setAttribute(ServerConstants.SessionKey.createdTime.name(), System.currentTimeMillis());

			LoginResponse response = new LoginResponse(ResponseConstants.SUCCESS, ResponseConstants.DT_200,
					ResponseConstants.DT_MSG_200);
			response.setData(getUserDetails(user));
			
			return response;
		}
		throw new InvalidException(ErrorConstants.DT_ERR_019, ErrorConstants.DT_ERR_MSG_019);

	}

	/**
	 * Logout API Call
	 * @return Response
	 * @throws UnauthorizedException
	 */
	public Response logout() throws UnauthorizedException {

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
			return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_201, ResponseConstants.DT_MSG_201);
		}
		throw new UnauthorizedException(ErrorConstants.DT_ERR_007, ErrorConstants.DT_ERR_MSG_007);
	}
	
	public LoginResponse testLogin(LoginDto loginDetails) throws InvalidException {

		User user = userDao.getUserDetail(loginDetails.getEmail());
		if (user == null) {
			userDao.createUser(loginDetails);
			user = userDao.getUserDetail(loginDetails.getEmail());
		}

		HttpSession session = request.getSession();
		session.setAttribute(ServerConstants.SessionKey.userId.name(), user.getUserId());
		session.setAttribute(ServerConstants.SessionKey.role.name(), user.getRole());
		session.setAttribute(ServerConstants.SessionKey.createdTime.name(), System.currentTimeMillis());

		LoginResponse response = new LoginResponse(ResponseConstants.SUCCESS, ResponseConstants.DT_200, ResponseConstants.DT_MSG_200);
		response.setData(getUserDetails(user));

		return response;
	}

	public Response updateTerms() throws InvalidException {
		
		User user = userDao.getUserDetail(handlerUtil.getEmail());
		if (user != null) {
			userDao.updateTermsCondition(user);
			
			return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_219, ResponseConstants.DT_MSG_219);
		}
		throw new UnauthorizedException(ErrorConstants.DT_ERR_007, ErrorConstants.DT_ERR_MSG_007);
	}
	
	private UserDetails getUserDetails(User user) {

		UserDetails detail = new UserDetails();
//		detail.setAuthId(user.getAuthId());
		detail.setEmail(user.getEmail());
		detail.setUserName(user.getName());
		detail.setUserId(user.getUserId());
		detail.setImageUrl(user.getImageUrl());
		detail.setRole(user.getRole());
		detail.setIsTermsAccepted(user.isTermsAccepted());
		
		return detail;
	}

}
