package com.tracker.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.constants.ErrorConstants;
import com.tracker.constants.ResponseConstants;
import com.tracker.dao.PlatformDao;
import com.tracker.dto.PlatformDeleteDto;
import com.tracker.dto.PlatformDto;
import com.tracker.dto.PlatformUpdateDto;
import com.tracker.dto.response.PlatformResponse;
import com.tracker.dto.response.Response;
import com.tracker.exception.InvalidException;
import com.tracker.util.HandlerUtility;

@Component
public class PlatformHandler {

	@Autowired
	private PlatformDao platformDao;
	
	@Autowired
	private HandlerUtility handlerUtil;

	public Response createPlaform(PlatformDto dto) throws InvalidException {

		platformDao.createPlatform(dto, handlerUtil.getEmail());
	
		return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_215, ResponseConstants.DT_MSG_215);
	}

	public PlatformResponse getPlatforms() {

		List<PlatformUpdateDto> list = platformDao.getPlatforms();
		
		PlatformResponse response = new PlatformResponse(ResponseConstants.SUCCESS, ResponseConstants.DT_216, ResponseConstants.DT_MSG_216);
		response.setData(list);
		
		return response;
	}

	public Response updatePlatform(PlatformUpdateDto dto) throws InvalidException {

		Boolean isUpdated = platformDao.updatePlatform(dto, handlerUtil.getEmail());
		if (isUpdated) {
			return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_217, ResponseConstants.DT_MSG_217);
		}
		throw new InvalidException(ErrorConstants.DT_ERR_021, ErrorConstants.DT_ERR_MSG_021);
	}

	public Response deletePlatform(PlatformDeleteDto dto) throws InvalidException {

		Boolean isDeleted = platformDao.deletePlatform(dto, handlerUtil.getEmail());
		if (isDeleted) {
			return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_218, ResponseConstants.DT_MSG_218);
		}
		throw new InvalidException(ErrorConstants.DT_ERR_021, ErrorConstants.DT_ERR_MSG_021);
	}

}
