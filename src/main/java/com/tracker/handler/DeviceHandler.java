package com.tracker.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.googlecode.objectify.ObjectifyService;
import com.tracker.constants.ErrorConstants;
import com.tracker.constants.ResponseConstants;
import com.tracker.constants.ServerConstants;
import com.tracker.dao.DeviceDao;
import com.tracker.dao.HistoryDao;
import com.tracker.dao.LockerDao;
import com.tracker.dao.ProjectDao;
import com.tracker.dao.UserDao;
import com.tracker.datastore.Device;
import com.tracker.datastore.History;
import com.tracker.datastore.Locker;
import com.tracker.datastore.Project;
import com.tracker.datastore.User;
import com.tracker.dto.BookedDetails;
import com.tracker.dto.DeviceDto;
import com.tracker.dto.DeviceReturnDto;
import com.tracker.dto.LockerDto;
import com.tracker.dto.response.BookedDeviceResponse;
import com.tracker.dto.response.DeviceResponse;
import com.tracker.dto.response.DeviceResponseDto;
import com.tracker.dto.response.HistoryDetails;
import com.tracker.dto.response.HistoryResponse;
import com.tracker.dto.response.Response;
import com.tracker.exception.InvalidException;
import com.tracker.util.HandlerUtility;

@Component
public class DeviceHandler {

	@Autowired
	private DeviceDao deviceDao;

	@Autowired
	private LockerDao locDao;

	@Autowired
	private HistoryDao historyDao;

	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private HandlerUtility handlerUtil;
		
	private static final String DEVICE_ID_KEY = "deviceId";

	public Response createDevice(DeviceDto dto) throws InvalidException {

		deviceDao.createDevice(dto, handlerUtil.getEmail());

		return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_202, ResponseConstants.DT_MSG_202);
	}

	public Response updateDevice(DeviceResponseDto dto) throws InvalidException {

		Boolean isUpdated = deviceDao.updateDevice(dto, handlerUtil.getEmail());
		if (isUpdated) {
			return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_204, ResponseConstants.DT_MSG_204);
		}
		throw new InvalidException(ErrorConstants.DT_ERR_012, ErrorConstants.DT_ERR_MSG_012);
	}

	public Response deleteDevice(DeviceReturnDto dto) throws InvalidException {

		Locker locker = locDao.findLocker(DEVICE_ID_KEY, dto.getDeviceId()); 
		if (locker == null) {
			Boolean isDeleted = deviceDao.deleteDevice(dto, handlerUtil.getEmail());
			if (isDeleted) {
				return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_205, ResponseConstants.DT_MSG_205);
			}
			throw new InvalidException(ErrorConstants.DT_ERR_012, ErrorConstants.DT_ERR_MSG_012);
		}
		throw new InvalidException(ErrorConstants.DT_ERR_036, ErrorConstants.DT_ERR_MSG_036);
	}

	/** History **/

	public HistoryResponse getHistory(int pageNum, long deviceId) throws InvalidException{
		
		if (pageNum < 1) 
			throw new InvalidException(ErrorConstants.DT_ERR_020, ErrorConstants.DT_ERR_MSG_020);
 
		List<History> list =  historyDao.getPageHistory(pageNum, deviceId);
		List<HistoryDetails> histories = new ArrayList<HistoryDetails>();

		for (History his : list) {

			HistoryDetails details = new HistoryDetails();
			details.setHistoryId(his.getHistoryId());
			details.setBookedTime(his.getBookedTime());
			details.setReturnedTime(his.getReturnedTime());

			Project project = projectDao.getProject(his.getProjectId());
			details.setProjectName(project.getName());
			
			User user = userDao.getUser(his.getUserId());
			details.setUserId(user.getUserId());
			details.setUserName(user.getName());
			histories.add(details);
		}
		HistoryResponse response = new HistoryResponse(ResponseConstants.SUCCESS, ResponseConstants.DT_206, ResponseConstants.DT_MSG_206);
		response.setData(histories);
		response.setTotalCount(historyDao.getAllHistoriesCount(deviceId));
		response.setPageLimit(ServerConstants.PAGE_LIMIT);

		return response;
	}

	/** Locker **/

	public Response createLocker(LockerDto dto) throws InvalidException {

		Device device = deviceDao.getDeviceEntity(dto.getDeviceId());
		if (device != null && device.isEnabled() && !device.isDeleted()) {

			Project project = projectDao.getProject(dto.getProjectId());
			if (project != null && !project.isDeleted()) {

				User user = handlerUtil.getUser();
				Locker locker = locDao.findLocker(DEVICE_ID_KEY, dto.getDeviceId());
				if (locker == null) {

					ObjectifyService.ofy().transact(() -> {

						locDao.createLocker(dto, ObjectifyService.ofy(), user.getUserId());
						//historyDao.createHistory(ObjectifyService.ofy(), user.getUserId(), device.getDeviceId(), project.getProjectId(), 0, true);
					});
					return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_207, ResponseConstants.DT_MSG_207);
				}
				throw new InvalidException(ErrorConstants.DT_ERR_014, ErrorConstants.DT_ERR_MSG_014);
			}
			throw new InvalidException(ErrorConstants.DT_ERR_017, ErrorConstants.DT_ERR_MSG_017);
		}
		throw new InvalidException(ErrorConstants.DT_ERR_012, ErrorConstants.DT_ERR_MSG_012);
	}

	public Response deleteLocker(DeviceReturnDto dto) throws InvalidException {

		Locker locker = locDao.findLocker(DEVICE_ID_KEY, dto.getDeviceId());
		if (locker != null) {

			User user = handlerUtil.getUser();
			if (locker.getUserId() == user.getUserId() || user.getRole().contentEquals(ServerConstants.Role.admin.name())) {
				
				ObjectifyService.ofy().transact(() -> {

					historyDao.createHistory(ObjectifyService.ofy(), locker.getUserId(), locker.getDeviceId(), locker.getProjectId(), locker.getUpdatedTime());
					ObjectifyService.ofy().delete().entity(locker).now();
				});
				return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_208, ResponseConstants.DT_MSG_208);
			}
			throw new InvalidException(ErrorConstants.DT_ERR_016, ErrorConstants.DT_ERR_MSG_016);
		}
		throw new InvalidException(ErrorConstants.DT_ERR_015, ErrorConstants.DT_ERR_MSG_015);
	}

	public BookedDeviceResponse getOccupiedDevices() {

		List<Object> list = locDao.occupiedDevices();
		List<BookedDetails> bookedList = new ArrayList<BookedDetails>();

		for (Object obj : list) {

			Locker locker = (Locker) obj;
			BookedDetails detail = new BookedDetails();
			detail.setLockerId(locker.getLockerId());

			Device device = deviceDao.getDeviceEntity(locker.getDeviceId());
			detail.setName(device.getName());
			detail.setDeviceId(device.getDeviceId());
			detail.setNickName(device.getNickName());
			detail.setColor(device.getColor());
			detail.setIpAddress(device.getIpAddress());
			detail.setUdid(device.getUdid());
			detail.setMacAddress(device.getMacAddress());
			detail.setCapacity(device.getCapacity());
			detail.setOsVersion(device.getOsVersion());
			detail.setComments(device.getComments());
			detail.setPlatformId(device.getPlatformId());
			detail.setIsAvailable(device.isEnabled());
			detail.setImei(device.getImei());
			detail.setDensity(device.getDensity());
			detail.setResolution(device.getResolution());
			detail.setSize(device.getSize());
			detail.setRam(device.getRam());
			
			User user = userDao.getUser(locker.getUserId());
			detail.setUserId(user.getUserId());
			detail.setUserName(user.getName());

			Project project = projectDao.getProject(locker.getProjectId());
			detail.setProjectId(project.getProjectId());
			detail.setProjectName(project.getName());

			bookedList.add(detail);
		}
		BookedDeviceResponse response = new BookedDeviceResponse(ResponseConstants.SUCCESS, ResponseConstants.DT_209, ResponseConstants.DT_MSG_209);
		response.setData(bookedList);
		
		return response;
	}
	
	public DeviceResponse getAvailableDevices() {
	
//		boolean isAdmin = handlerUtil.getUser().getRole().equals(ServerConstants.Role.admin.name());
		
		List<Object> devices;
//		if (isAdmin) {
			devices = deviceDao.getDevices();
//		} else {
//			devices = deviceDao.getAvailableDevices();
//		}
		
		List<Object> bookedDevices = locDao.occupiedDevices();
		List<Object> deviceList = new ArrayList<>();
		deviceList.addAll(devices);

		for (Object obj : devices) {

			Device device = (Device) obj;
			for (Object bookObj : bookedDevices) {
				Locker loc = (Locker) bookObj;

				if(loc.getDeviceId() == device.getDeviceId()) {
					deviceList.remove(obj);
					break;
				}
			}
		}
		List<DeviceResponseDto> availableDevices = deviceDao.prepareDeviceList(deviceList);
		DeviceResponse response = new DeviceResponse(ResponseConstants.SUCCESS, ResponseConstants.DT_210, ResponseConstants.DT_MSG_210);
		response.setData(availableDevices);

		return response;
	}
}
