package com.tracker.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.dao.DeviceDao;
import com.tracker.dao.LockerDao;
import com.tracker.dao.UserDao;
import com.tracker.datastore.Device;
import com.tracker.datastore.Locker;
import com.tracker.datastore.User;
import com.tracker.util.SGMailHandler;

@Component
public class NotifyUserHandler {

	@Autowired
	private LockerDao lockerDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private DeviceDao deviceDao;
	
	public void notifyUserToReturnDevice() {
		
		List<Object> list = lockerDao.occupiedDevices();
		SGMailHandler mailHandler = new SGMailHandler();

		for (Object obj : list) {

			Locker locker = (Locker) obj;
			
			Device device = deviceDao.getDeviceEntity(locker.getDeviceId());
			User user = userDao.getUser(locker.getUserId());

			String subject = "Reminder: Return " + device.getNickName() + " device";			
			
			String body = "<html>\n" + 
					"<head>\n" + 
					"</head>\n" + 
					"<body>\n" + 
					"<p>Hi " + user.getName() + ",</p>\n" + 
					"<p>Gentle reminder! </p>\n" + 
					"<p>\n Please return the "+ device.getName() + " (" + device.getNickName() + ") device before you leave the office.</p>\n" +
					"<p>Regards,</p>\n" + 
					"<p>Device Tracker Team</p>\n" + 
					"</body>\n" + 
					"</html>\n";
			
			String toAddress = user.getEmail();
			
			mailHandler.send(toAddress, subject, body);
		}
	}
}
