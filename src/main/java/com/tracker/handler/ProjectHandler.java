package com.tracker.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.constants.ErrorConstants;
import com.tracker.constants.ResponseConstants;
import com.tracker.constants.ServerConstants;
import com.tracker.dao.LockerDao;
import com.tracker.dao.ProjectDao;
import com.tracker.datastore.Locker;
import com.tracker.dto.ProjectDeleteDto;
import com.tracker.dto.ProjectDto;
import com.tracker.dto.ProjectUpdateDto;
import com.tracker.dto.response.ProjectDetail;
import com.tracker.dto.response.ProjectResponse;
import com.tracker.dto.response.Response;
import com.tracker.exception.InvalidException;
import com.tracker.util.HandlerUtility;

@Component
public class ProjectHandler {

	@Autowired
	private ProjectDao projectDao;
	
	@Autowired
	private HandlerUtility handlerUtil;
	
	@Autowired
	private LockerDao locDao;
	
	private static final String PROJECT_ID_KEY = "projectId";

	public Response createProject(ProjectDto projectDetails) throws InvalidException {

		projectDao.createProject(projectDetails, handlerUtil.getEmail());

		return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_211, ResponseConstants.DT_MSG_211);
	}

	public ProjectResponse getProjects() {

		List<ProjectDetail> list = projectDao.getProjects();
		
		ProjectResponse response = new ProjectResponse(ResponseConstants.SUCCESS, ResponseConstants.DT_212, ResponseConstants.DT_MSG_212);
		response.setData(list);
		
		return response;
	}

	public Response updateProject(ProjectUpdateDto projectDetails) throws InvalidException {

		Boolean isUpdated = projectDao.updateProject(projectDetails, handlerUtil.getEmail());
		if (isUpdated) {
			return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_213, ResponseConstants.DT_MSG_213);
		}
		throw new InvalidException(ErrorConstants.DT_ERR_017, ErrorConstants.DT_ERR_MSG_017);
	}

	public Response deleteProject(ProjectDeleteDto projectDetails) throws InvalidException {

		Locker locker = locDao.findLocker(PROJECT_ID_KEY, projectDetails.getProjectId()); 
		if (locker == null) {
			Boolean isDeleted = projectDao.deleteProject(projectDetails, handlerUtil.getEmail());
			if (isDeleted) {
				return new Response(ResponseConstants.SUCCESS, ResponseConstants.DT_214, ResponseConstants.DT_MSG_214);
			}
			throw new InvalidException(ErrorConstants.DT_ERR_017, ErrorConstants.DT_ERR_MSG_017);
		}
		throw new InvalidException(ErrorConstants.DT_ERR_037, ErrorConstants.DT_ERR_MSG_037);
	}
}
