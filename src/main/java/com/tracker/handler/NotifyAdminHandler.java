package com.tracker.handler;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracker.dao.DeviceDao;
import com.tracker.dao.LockerDao;
import com.tracker.dao.ProjectDao;
import com.tracker.dao.UserDao;
import com.tracker.datastore.Device;
import com.tracker.datastore.Locker;
import com.tracker.datastore.Project;
import com.tracker.datastore.User;
import com.tracker.util.SGMailHandler;

@Component
public class NotifyAdminHandler {

	@Autowired
	private LockerDao lockerDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private DeviceDao deviceDao;
	
	@Autowired
	private ProjectDao projectDao;
	
	public void notifyDailyReports() {
		
		List<Object> list = lockerDao.occupiedDevices();

		if (list.isEmpty()) return;
		
		SGMailHandler mailHandler = new SGMailHandler();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy' 'hh:mm a");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		StringBuilder body = new StringBuilder("");

		String bodyMessage = "<html>\n" + 
				"<head>\n" + 
				"<style>\n" + 
				"table {\n" + 
				"  font-family: arial, sans-serif;\n" + 
				"  border-collapse: collapse;\n" + 
				"  width: 100%;\n" + 
				"}\n" + 
				"\n" + 
				"td, th {\n" + 
				"  border: 1px solid #dddddd;\n" + 
				"  text-align: left;\n" + 
				"  padding: 8px;\n" + 
				"}\n" + 
				"\n" + 
				"tr:nth-child(even) {\n" + 
				"  background-color: #dddddd;\n" + 
				"}\n" + 
				"</style>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"\n" + 
				"<p>Hi Admin,</p>\n" + 
				"<p>Please find the today's device report. You can find the not returned device list in the given table. </p>\n" + 

				"\n" + 
				"<table>\n" + 
				"  <tr>\n" + 
				"    <th>Device</th>\n" + 
				"    <th>Name</th>\n" + 
				"    <th>Project</th>\n" + 
				"    <th>Taken Time</th>\n" + 
				"  </tr>\n"; 

		body.append(bodyMessage);

		for (Object obj : list) {

			Locker locker = (Locker) obj;

			Device device = deviceDao.getDeviceEntity(locker.getDeviceId());
			User user = userDao.getUser(locker.getUserId());
			Project project = projectDao.getProject(locker.getProjectId());			
	        
			String row = "<tr>" + 
					"    <td>" + device.getName() + "</td>" + 
					"    <td>" + user.getName() + "</td>" + 
					"    <td>" + project.getName() + "</td>" + 
					"    <td>" + simpleDateFormat.format(locker.getUpdatedTime()) + "</td>" + 
					"  </tr>";
			
			body.append(row);
		}
		
		String bottom = "</table>" + 
				"<p>Regards,</p>\n" + 
				"<p>Device Tracker Team</p>\n" + 
				"</body>\n" + 
				"</html>\n";

		body.append(bottom);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");

		String subject = "Device Daily Report - "+dateFormat.format(System.currentTimeMillis());
		
		List<User> adminUsers = userDao.getAdminUsers();
		
		for (User user : adminUsers) {
			
			mailHandler.send(user.getEmail(), subject, body.toString());
		}		
		
	}

}
