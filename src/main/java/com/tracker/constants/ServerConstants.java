package com.tracker.constants;

import java.util.regex.Pattern;

public class ServerConstants {

	public enum Role {
		user, admin
	}

	public enum SessionKey {
		userId, role, createdTime
	}

	public static final String STORAGE_NAME = "live_device_tracker";// For live
	//public static final String STORAGE_NAME = "device_tracker";// For dev
	public static final String CLIENT_ID = "603598462360-k347kqjt2m668him81d1iuei75nss75f.apps.googleusercontent.com";

	public static final String SG_API_KEY = "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";
	public static final String SG_MAIL_SENDER = "softwareadmin@tringapps.com";

	public static final long SESSION_EXPIRY_INTERVAL = 24 * 60 * 60 * 1000; // One day

	public static final Pattern EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	public static final String VALID_EMAIL = "@tringapps.com";

	public static final String[] ADMIN_ENDPOINTS = { "device/register", "device/update", "device/delete",
			"device/history", "device/list/all", "project/register", "project/update", "project/delete",
			"platform/register", "platform/update", "platform/delete" };

	public static final int PAGE_LIMIT = 10;

}
