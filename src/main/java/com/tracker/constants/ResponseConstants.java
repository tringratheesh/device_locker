package com.tracker.constants;

public class ResponseConstants {

	/**
	 * Success Constants
	 */
	
	public static final String SUCCESS = "SUCCESS";
	
	/**
	 * User response constants
	 */
	
	public static final String DT_200 = "DT_CODE_200";
	public static final String DT_MSG_200 = "You have succesfully logged In!";
	
	public static final String DT_201 = "DT_CODE_201";
	public static final String DT_MSG_201 = "You have succesfully logged out!";

	public static final String DT_202 = "DT_CODE_202";
	public static final String DT_MSG_202 = "Device has been registered successfully!";

	public static final String DT_219 = "DT_CODE_219";
	public static final String DT_MSG_219 = "You have succesfully accepted terms and condition!";

	/**
	 * Device response constants
	 */
	
	public static final String DT_203 = "DT_CODE_203";
	public static final String DT_MSG_203 = "You have retrieved device list successfully!";

	public static final String DT_204 = "DT_CODE_204";
	public static final String DT_MSG_204 = "Device has been updated successfully!";

	public static final String DT_205 = "DT_CODE_205";
	public static final String DT_MSG_205 = "Device has been deleted successfully!";

	public static final String DT_206 = "DT_CODE_206";
	public static final String DT_MSG_206 = "Successfully retrieved device history informations!";

	public static final String DT_207 = "DT_CODE_207";
	public static final String DT_MSG_207 = "You have booked the device successfully!";

	public static final String DT_208 = "DT_CODE_208";
	public static final String DT_MSG_208 = "You have returned the device successfully!";

	public static final String DT_209 = "DT_CODE_209";
	public static final String DT_MSG_209 = "You have retrieved booked device list successfully!";

	public static final String DT_210 = "DT_CODE_210";
	public static final String DT_MSG_210 = "You have retrieved available device list successfully!";

	/**
	 * Project response constants
	 */
	
	public static final String DT_211 = "DT_CODE_211";
	public static final String DT_MSG_211 = "Project has been registered successfully!";

	public static final String DT_212 = "DT_CODE_212";
	public static final String DT_MSG_212 = "You have retrieved project list successfully!";

	public static final String DT_213 = "DT_CODE_213";
	public static final String DT_MSG_213 = "Project has been updated successfully!";

	public static final String DT_214 = "DT_CODE_214";
	public static final String DT_MSG_214 = "Project has been deleted successfully!";

	/**
	 * Platform response constants
	 */
	
	public static final String DT_215 = "DT_CODE_215";
	public static final String DT_MSG_215 = "Platform has been registered successfully!";

	public static final String DT_216 = "DT_CODE_216";
	public static final String DT_MSG_216 = "You have retrieved platform list successfully!";

	public static final String DT_217 = "DT_CODE_217";
	public static final String DT_MSG_217 = "Platform has been updated successfully!";

	public static final String DT_218 = "DT_CODE_218";
	public static final String DT_MSG_218 = "Platform has been deleted successfully!";

}
