package com.tracker.constants;

public class ErrorConstants {
	
	public static final String ERROR = "ERROR";
	
	public static final String DT_ERR_001 = "DT_ERR_CODE_001";
	public static final String DT_ERR_MSG_001 = "Token id is required!";
	
	public static final String DT_ERR_002 = "DT_ERR_CODE_002";
	public static final String DT_ERR_MSG_002 = "Email id is required!";

	public static final String DT_ERR_003 = "DT_ERR_CODE_003";
	public static final String DT_ERR_MSG_003 = "Name is required!";

	public static final String DT_ERR_004 = "DT_ERR_CODE_004";
	public static final String DT_ERR_MSG_004 = "Email id is not valid format!";

	public static final String DT_ERR_005 = "DT_ERR_CODE_005";
	public static final String DT_ERR_MSG_005 = "Provide valid organization email id!";
	
	public static final String DT_ERR_006 = "DT_ERR_CODE_006";
	public static final String DT_ERR_MSG_006 = "Invalid google tokenId! Please try again!";

	public static final String DT_ERR_007 = "DT_ERR_CODE_007";
	public static final String DT_ERR_MSG_007 = "Session is not valid plaese login!";
	
	public static final String DT_ERR_008 = "DT_ERR_CODE_008";
	public static final String DT_ERR_MSG_008 = "Device nick name is required!";

	public static final String DT_ERR_009 = "DT_ERR_CODE_009";
	public static final String DT_ERR_MSG_009 = "Device id is required!";

	public static final String DT_ERR_010 = "DT_ERR_CODE_010";
	public static final String DT_ERR_MSG_010 = "Project name is required!";

	public static final String DT_ERR_011 = "DT_ERR_CODE_011";
	public static final String DT_ERR_MSG_011 = "Project id is required!";

	public static final String DT_ERR_012 = "DT_ERR_CODE_012";
	public static final String DT_ERR_MSG_012 = "Device not available. Please contact admin.";

	public static final String DT_ERR_013 = "DT_ERR_CODE_013";
	public static final String DT_ERR_MSG_013 = "Project id is not valid!";

	public static final String DT_ERR_014 = "DT_ERR_CODE_014";
	public static final String DT_ERR_MSG_014 = "Device already picked by someone!";

	public static final String DT_ERR_015 = "DT_ERR_CODE_015";
	public static final String DT_ERR_MSG_015 = "Device is not yet booked!";

	public static final String DT_ERR_016 = "DT_ERR_CODE_016";
	public static final String DT_ERR_MSG_016 = "Device can return only specific user or admin!";

	public static final String DT_ERR_017 = "DT_ERR_CODE_017";
	public static final String DT_ERR_MSG_017 = "Project not available. Please contact admin.";
	
	public static final String DT_ERR_018 = "DT_ERR_CODE_018";
	public static final String DT_ERR_MSG_018 = "Unknown error! Please try again";

	public static final String DT_ERR_019 = "DT_ERR_CODE_019";
	public static final String DT_ERR_MSG_019 = "Invalid email!";

	public static final String DT_ERR_020 = "DT_ERR_CODE_020";
	public static final String DT_ERR_MSG_020 = "Enter valid page number!";
	
	public static final String DT_ERR_021 = "DT_ERR_CODE_021";
	public static final String DT_ERR_MSG_021 = "Platform is not exists! Please enter valid platform details!";

	public static final String DT_ERR_022 = "DT_ERR_CODE_022";
	public static final String DT_ERR_MSG_022 = "Platform name is required!";

	public static final String DT_ERR_023 = "DT_ERR_CODE_023";
	public static final String DT_ERR_MSG_023 = "Platform id is required!";

	public static final String DT_ERR_024 = "DT_ERR_CODE_024";
	public static final String DT_ERR_MSG_024 = "Duplicated platform name! please use different name.";

	public static final String DT_ERR_025 = "DT_ERR_CODE_025";
	public static final String DT_ERR_MSG_025 = "Duplicated project name! please use different name.";

	public static final String DT_ERR_026 = "DT_ERR_CODE_026";
	public static final String DT_ERR_MSG_026 = "Duplicated device nick name! please use different nick name.";

	public static final String DT_ERR_027 = "DT_ERR_CODE_027";
	public static final String DT_ERR_MSG_027 = "Duplicated device ip address! please use different ip address.";

	public static final String DT_ERR_028 = "DT_ERR_CODE_028";
	public static final String DT_ERR_MSG_028 = "Duplicated device Mac address! please use different Mac address.";

	public static final String DT_ERR_029 = "DT_ERR_CODE_029";
	public static final String DT_ERR_MSG_029 = "Platform is not exists! Please enter valid platform id!";

	public static final String DT_ERR_030 = "DT_ERR_CODE_030";
	public static final String DT_ERR_MSG_030 = "IP Address is required!";
	
	public static final String DT_ERR_031 = "DT_ERR_CODE_031";
	public static final String DT_ERR_MSG_031 = "MAC Address is required!";

	public static final String DT_ERR_032 = "DT_ERR_CODE_032";
	public static final String DT_ERR_MSG_032 = "OS is required!";

	public static final String DT_ERR_033 = "DT_ERR_CODE_033";
	public static final String DT_ERR_MSG_033 = "Capacity is required!";

	public static final String DT_ERR_034 = "DT_ERR_CODE_034";
	public static final String DT_ERR_MSG_034 = "Colour is required!";

	public static final String DT_ERR_035 = "DT_ERR_CODE_035";
	public static final String DT_ERR_MSG_035 = "Model name is required!";

	public static final String DT_ERR_036 = "DT_ERR_CODE_036";
	public static final String DT_ERR_MSG_036 = "Device is not in locker. Please return the device to locker then proceed the delete";

	public static final String DT_ERR_037 = "DT_ERR_CODE_037";
	public static final String DT_ERR_MSG_037 = "Someone booked the device using this project. Please return it and proceed the delete.";

}
