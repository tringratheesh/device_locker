package com.tracker.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.googlecode.objectify.ObjectifyService;
import com.tracker.datastore.Device;
import com.tracker.datastore.History;
import com.tracker.datastore.Locker;
import com.tracker.datastore.Platform;
import com.tracker.datastore.Project;
import com.tracker.datastore.Session;
import com.tracker.datastore.User;

public class ObjectifyConfig implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		ObjectifyService.begin();
		
		ObjectifyService.register(Device.class);
		ObjectifyService.register(History.class);
		ObjectifyService.register(Locker.class);
		ObjectifyService.register(Project.class);
		ObjectifyService.register(Session.class);
		ObjectifyService.register(User.class);
		ObjectifyService.register(Platform.class);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
	}

}
