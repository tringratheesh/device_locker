package com.tracker.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.tracker.filter.AuthenticationFilter;

public class WebServletConfiguration implements WebApplicationInitializer {

	@Override	
	public void onStartup(ServletContext ctx) {
		
		AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
		webCtx.register(SpringConfig.class);
		webCtx.setServletContext(ctx);
		
		ctx.addListener(new ContextLoaderListener(webCtx));
		ctx.addListener(new ObjectifyConfig());
		ServletRegistration.Dynamic servlet = ctx.addServlet("dispatcher", new DispatcherServlet(webCtx));
		servlet.setLoadOnStartup(1);
		servlet.addMapping("/");
		
		ctx.addFilter("AuthenticationFiler", AuthenticationFilter.class).addMappingForServletNames(null, false, "dispatcher");
	}
}
